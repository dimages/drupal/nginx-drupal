#!/bin/bash

if [ ! -e "/etc/nginx/conf.d/drupal.conf" ]; then

  cp /etc/nginx/conf.d/default-drupal.conf /etc/nginx/conf.d/drupal.conf

  export DOMAIN=${DOMAIN}
  export UPSTREAM=${UPSTREAM}

  sed -i "s/website.tld/${DOMAIN}/" /etc/nginx/conf.d/drupal.conf
  sed -i "s/localhost/${UPSTREAM}/" /etc/nginx/conf.d/drupal.conf

  echo "Nginx has just been configured"

else
  echo "Entrypoint initialization is already finished"
fi

rm -f /etc/nginx/conf.d/default-drupal.conf

exec "$@"
