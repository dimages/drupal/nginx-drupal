# Export other vars in your shell environment for testing
#
# export NAME=website
# export TLD=com
# export UPSTREAM=helm-name-php
#
VERSION=1

login:
	docker login -u ${USERNAME} -p ${PASSWORD}


new-version: build tag push

build:
	docker build -t registry.gitlab.com/dimages/nginx-drupal:$VERSION .

tag:
	docker tag registry.gitlab.com/dimages/nginx-drupal:$VERSION registry.gitlab.com/dimages/nginx-drupal:latest

push:
	docker push registry.gitlab.com/dimages/nginx-drupal:${VERSION}
	docker push registry.gitlab.com/dimages/nginx-drupal:latest


run:
	docker run -it -e WEBSITE_DOMAIN=${WEBSITE_DOMAIN} -e WEBSITE_UPSTREAM=${WEBSITE_UPSTREAM} registry.gitlab.com/dimages/nginx-drupal:latest