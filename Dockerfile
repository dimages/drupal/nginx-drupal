FROM nginx:latest

COPY config/nginx.conf /etc/nginx/nginx.conf
COPY config/default.conf /etc/nginx/conf.d/default.conf
COPY config/drupal.conf /etc/nginx/conf.d/default-drupal.conf
COPY entrypoint.sh /usr/local/bin

RUN chmod +x /usr/local/bin/entrypoint.sh && \
    apt-get update && \
    apt-get install vim -y

ENTRYPOINT ["entrypoint.sh"]
WORKDIR /etc/nginx/conf.d
CMD ["nginx", "-g", "daemon off;"]
